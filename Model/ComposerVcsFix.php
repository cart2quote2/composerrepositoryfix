<?php

namespace Cart2Quote\ComposerRepositoryFix\Model;

/**
 * Fix Magento to allow to read VCS.
 *
 * 1) When the info command is executed by Magento the backspace is not applied.
 * Due to this, Magento can't read the 'name' key from the array and return the error: Undefined index: name.
 * This fix allows Magento to read the result of the composer info command correctly.
 *
 * 2) Local repositories are not compatible due to the evaluation of the composer.json in the var folder instead of the Magento root folder.
 * Paths are relative to the root and will not exist in the var folder.
 *
 * Class Fix
 * @package Cart2Quote\ComposerRepositoryFix\Model
 */
class Fix
{

    /**
     * Path the to InfoCommand
     */
    const PATH_TO_INFO_COMMAND = '/vendor/magento/composer/src/InfoCommand.php';

    /**
     * Path to InfoCommand File
     */
    const PATH_TO_INFO_COMMAND_FILE = '/vendor/magento/composer/src';

    /**
     * Path the flag
     */
    const PATH_TO_FLAG = '/applied.flag';

    const SYM_LINK_TO_VAR = '/var/var';

    const LINK_TO_VAR = '/var';

    /**
     * Apply the fix
     *
     * @throws \Exception
     */
    function apply()
    {
        if (!$this->isApplied()) {
            if (!$this->checkWritableFlag()) {
                throw new \Exception('Module "cart2quote/module-composer-repository-fix": ' .
                    'Unable to apply the fix, PHP can\'t write to: '. __DIR__);
            }

            if (!$this->checkWritableInfoCommand()) {
                throw new \Exception('Module "cart2quote/module-composer-repository-fix": ' .
                    'Unable to apply the fix, PHP can\'t write to: '. $this->getInfoCommandFilePath());
            }


            $this->applyFix();
            $this->createVarSymLink();
            $this->placeFlag();
        }
    }

    /**
     * Checks if the fix is already applied
     *
     * @return bool
     */
    public function isApplied()
    {
        return file_exists(__DIR__ . '/applied.flag');
    }

    /**
     * Fix the relative paths by SymLink
     *
     * @return void
     */
    public function createVarSymLink()
    {
        symlink(
            BP . self::LINK_TO_VAR, 
            BP . self::SYM_LINK_TO_VAR
        );
    }

    /**
     * Checks if the InfoCommand PHP file is writable
     *
     * @return bool
     */
    public function checkWritableInfoCommand()
    {
        return is_writable($this->getInfoCommandFilePath());
    }

    /**
     * Checks if the flag is writable
     *
     * @return bool
     */
    public function checkWritableFlag()
    {
        return is_writable(__DIR__);
    }

    /**
     * Get the contents of the InfoCommand file
     *
     * @return string
     */
    private function getInfoCommandContents()
    {
        return file_get_contents($this->getInfoCommandPath());
    }

    /**
     * Apply the fix
     *
     * @return void
     */
    private function applyFix()
    {
        $fileContents = str_replace(
            $this->getOriginalCodeSearch(),
            $this->getFixCode() . "\n" . $this->getOriginalCodeSearch(),
            $this->getInfoCommandContents()
        );

        file_put_contents(
            $this->getInfoCommandPath(),
            $fileContents
        );
    }

    /**
     * Place the flag
     *
     * @return void
     */
    private function placeFlag()
    {
        file_put_contents(
            $this->getFlagPath(),
            ''
        );
    }

    /**
     * Get the original code for the search and replace
     *
     * @return string
     */
    private function getOriginalCodeSearch()
    {
        return "\$rawLines = explode(\"\n\", str_replace(\"\r\n\", \"\n\", \$output));";
    }

    /**
     * Get the code for the fix
     *
     * @return string
     */
    private function getFixCode()
    {
        return "\$output = str_replace(\"\x08\", \"\n\", \$output);";
    }

    /**
     * Get the InfoCommand path
     *
     * @return string
     */
    private function getInfoCommandPath()
    {
        return BP . self::PATH_TO_INFO_COMMAND;
    }

    /**
     * Get the InfoCommand file path
     *
     * @return string
     */
    private function getInfoCommandFilePath()
    {
        return BP . self::PATH_TO_INFO_COMMAND_FILE;
    }

    /**
     * Get the flag path
     *
     * @return string
     */
    private function getFlagPath()
    {
        return __DIR__ . self::PATH_TO_FLAG;
    }

}
