<?php
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Cart2Quote_ComposerRepositoryFix',
    __DIR__
);

include_once (__DIR__ . '/Model/ComposerVcsFix.php');

$fix = new \Cart2Quote\ComposerRepositoryFix\Model\Fix();
$fix->apply();