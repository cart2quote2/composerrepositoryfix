========= LICENSE =========

CART2QUOTE CONFIDENTIAL

[2009] - [2017] Cart2Quote B.V.
All Rights Reserved.
https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)

NOTICE OF LICENSE

All information contained herein is, and remains
the property of Cart2Quote B.V. and its suppliers,
if any.  The intellectual and technical concepts contained
herein are proprietary to Cart2Quote B.V.
and its suppliers and may be covered by European and Foreign Patents,
patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material
is strictly forbidden unless prior written permission is obtained
from Cart2Quote B.V.


========= SUMMARY =========

The purpose of the ComposerRepositoryFix module is to fix the System Upgrade page of Magento.
This fix will allow you to:
1. Use a VCS repository in combination with Magento System Upgrade
2. Use an Artifact repository in combination with Magento System Upgrade

========= REQUIREMENTS =========

MAGENTO:        2.*

========= INSTALLATION =========

1.      composer require cart2quote/module-composer-repository-fix

2.      php -f bin/magento module:enable Cart2Quote_ComposerRepositoryFix

3.      php bin/magento setup:upgrade

4.      rm -rf var/generation/* var/cache/* pub/static/frontend/* pub/static/adminhtml/* var/page_cache/* var/di/* var/di

5.      php -f bin/magento setup:di:compile

6.      php -f bin/magento setup:static-content:deploy

7.      php -f bin/magento indexer:reindex

========= CONTACT =========

Request a support ticket:
https://cart2quote.zendesk.com/hc/en-us/requests/new

Request a customization:
https://www.cart2quote.com/magento-customizations